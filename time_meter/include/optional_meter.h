/*
Copyright (c) 2013 J. Daniel Garcia <josedaniel.garcia@uc3m.es>

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*/
#ifndef TIME_METER_OPTIONAL_METER_H
#define TIME_METER_OPTIONAL_METER_H

namespace xul {

namespace time_meter {

template <class M, bool COND>
class optional_meter_base : private M
{
public:
  void start() { M::start(); }
  void stop() { M::stop(); }
  template <typename U>
    unsigned int count() { return M::template count<U>(); }
  constexpr static bool is_active() { return true; }
};

template <class M>
class optional_meter_base<M, false> {
public:
  void start() { }
  void stop() { }
  template <typename U>
    unsigned int count() { return {}; }
  constexpr static bool is_active() { return false; }
};

#if defined(XUL_TIME_METER_ENABLED) && !defined(XUL_TIMER_DISABLED)
template <class M>
using optional_meter = optional_meter_base<M,true>;
#elif defined(XUL_TIME_METER_DISABLED) && !defined(XUL_TIMER_ENABLED)
template <class M>
using optional_meter = optional_meter_base<M,false>;
#else
static_assert(false, "Must set -DXUL_TIME_METER_ENABLED xor -DXUL_TIME_METER_DISABLED in compiler flags");
#endif

}

}

#endif
