/*
Copyright (c) 2013 J. Daniel Garcia <josedaniel.garcia@uc3m.es>

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
 */
#ifndef TIME_METER_SYSTEM_METER_H
#define TIME_METER_SYSTEM_METER_H

#include <chrono>

namespace xul {

namespace time_meter {

template <class CLK>
class system_meter {
public:
	using time_point = typename CLK::time_point;
	using duration = typename CLK::duration;

private:
	CLK clock;
	time_point t1, t2;

public:
	void start() { t1 = t2 = CLK::now(); }
	void stop() { t2 = CLK::now(); }

        template <typename U>
        unsigned int count() {
          using namespace std::chrono;
          auto dif = t2-t1;
          return duration_cast<U>(dif).count();
        }
};

} // namespace time_meter

} // namespace xul

#endif
