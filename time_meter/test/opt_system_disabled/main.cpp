/*
Copyright (c) 2013 J. Daniel Garcia <josedaniel.garcia@uc3m.es>

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
 */
#include <iostream>
#include <sstream>
#include <thread>
#include "system_meter.h"
#include "optional_meter.h"

int main(int argc, char ** argv) {
  using namespace xul::time_meter;
  using namespace std;

  if (argc != 2) {
    cerr << "Wrong format" << endl;
    cerr << argv[0] << " milisecs" << endl;
    return 1;
  }

  unsigned int delay;
  istringstream(argv[1]) >> delay;
  cout << "Delay: " << delay << endl;

  optional_meter<system_meter<chrono::system_clock>>  m;
  m.start();
  this_thread::sleep_for(chrono::milliseconds(delay));
  m.stop();
  if (m.is_active()) {
    cout << "Time: " << m.count<chrono::milliseconds>() << " milliseconds" << endl;
  }

  return 0;
}
