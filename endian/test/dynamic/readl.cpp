/*
Copyright (c) 2013 J. Daniel Garcia <josedaniel.garcia@uc3m.es>

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
 */
#include "endian_converter.h"
#include <fstream>
#include <iostream>

template<xul::endian::endian_type ET>
void read_file(std::string fname)
{
  using namespace std;
  using namespace xul::endian;

  ifstream file(fname, ios::binary);
  dynamic_endian_converter<ET> ec;

  byte_sequence<4> x;
  x.read(file);
  byte_sequence<4> y;
  y.read(file);

  int32_t xvalue = ec.template to_host<int32_t>(x);
  float yvalue = ec.template to_host<float>(y);

  cout << xvalue << endl;
  cout << yvalue << endl;

}

int main() {
  using namespace xul::endian;
  read_file<endian_type::little>("little.dat");
  read_file<endian_type::big>("big.dat");

  return 0;
}
