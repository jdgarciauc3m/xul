#!/bin/bash

check_source_file()
{
diff -s <(head -n `expr $LICLEN + 1` $1 | tail -n $LICLEN) $LICFILENAME &>/dev/null
if test $? -ne 0
then
  echo "Missing license in File: " $1
fi
}

check_directory()
{
echo "Checking directory $1"
for i in $1/*
do
  if [ -d $i ]
  then
    if test `basename $i` != build
    then
      check_directory $i
    fi
  else
    case $i in
      *.cpp) check_source_file $i ;;
      *.h) check_source_file $i ;;
    esac
  fi
done
}

if test $# -ne 1
then
  echo "Error: Missing argument" >&2
  echo "Format: " $0 " <dir>" >&2
  exit 1
fi

LICFILENAME="$1/LICENSE.txt"

if [ -f "$LICFILENAME" ] 
then
  LICLEN=`wc -l $LICFILENAME | sed "s/^ +//" | sed "s/ .*//"`
else
  echo "Missing license filename: $LICFILENAME" >&2
  exit
fi

echo "Starting..."

check_directory $1 
